function prism_line_highlight(element, ranges) {
    "use strict";

    // ensure "element" is DOM node
    if (typeof element === "string") {
        if (element.substr(0, 1) === "#") {
            element = element.substr(1);
        }
        element = document.getElementById(element);
        if (element === null) {
            return false;
        }
    } else if (typeof element === "object" && typeof element.length === "number") {
        element = element[0];
    }

    // remove existing ".line-highlight" nodes
    var classes;
    element.childNodes.forEach(function (child) {
        classes = child.className.trim().split(" ");
        if (classes.indexOf("line-highlight") > -1) {
            element.removeChild(child);
        }
    });

    // check "ranges" is defined
    if (ranges !== undefined) {
        // ensures "ranges" is object
        if (typeof ranges !== "object") {
            ranges = String(ranges);
            ranges = ranges.trim().split(",");
        }

        var highlight;
        var height;
        ranges.forEach(function (range) {
            range = range.trim().split("-");
            highlight = document.createElement("div");

            // convert singles to ranges
            if (range.length === 1) {
                range.push(range[0]);
            }

            // append new highlights
            if (range.length === 2) {
                range[0] = parseInt(range[0]);
                range[1] = parseInt(range[1]);
                if (range[0] <= range[1]) {
                    height = range[1] - range[0];
                    highlight.setAttribute("aria-hidden", true);
                    highlight.classList.add("line-highlight");
                    highlight.style.top = ((range[0] - 1) * 24) + "px";
                    highlight.style.height = ((height + 1) * 24) + "px";

                    // update "data-range" attribute
                    if (height) {
                        range = range.join("-");
                    } else {
                        range = range[0];
                    }
                    highlight.setAttribute("data-range", range);
                    element.appendChild(highlight);
                }
            }
        });
    }
}
